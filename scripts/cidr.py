import subprocess
from json import loads

bashCommand = "aws ec2 describe-subnets"
process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
subnets, error = process.communicate()

subnets = loads(subnets.decode())

avail_blocks = list(range(16))

for subnet in subnets['Subnets']:
    block = int(subnet["CidrBlock"].split('.')[2]) / 16
    avail_blocks.remove(block)

if len(avail_blocks) < 5:
    raise ValueError('No available CIDR blocks.')

subnet_blocks = avail_blocks[:5]
cidr_prefix = '.'.join(subnets['Subnets'][0]['CidrBlock'].split('.')[:2])

picked_subnets = ['{}.{}.0/20'.format(cidr_prefix, block * 16) for block in subnet_blocks]

for idx, subnet in enumerate(picked_subnets):
    if idx < 3:
        print("export TF_VAR_subnet_{}='{}'".format(chr(97 + idx), subnet))
    elif idx == 3:
        print("export GNAR_KOPS_NETWORK_CIDR='{}'".format(subnet))
    else:
        print("kops_subnet_cidr = '{}'".format(subnet))
