output "certificate_arn" {
  value = "${aws_acm_certificate.cert.arn}"
}

output "db_endpoint" {
  value = "${aws_db_instance.gnar.address}"
}

output "domain_nameserver_records" {
  value = "${aws_route53_zone.root.name_servers}"
}

output "environment_variables" {
  value = <<STRING

>>>>> Start environment variables <<<<<

export GNAR_ECR_URI_OFF_PISTE='${aws_ecr_repository.off-piste.repository_url}'
export GNAR_ECR_URI_PISTE='${aws_ecr_repository.piste.repository_url}'
export GNAR_ECR_URI_POWDER='${aws_ecr_repository.powder.repository_url}'

export GNAR_EMAIL_HOST='app.${var.domain}'
export GNAR_EMAIL_LOGO_URL='https://${aws_s3_bucket.app.bucket_domain_name}/${aws_s3_bucket_object.logo.key}'

export GNAR_PG_ENDPOINT='${aws_db_instance.gnar.address}'

export GNAR_SES_ACCESS_KEY_ID='${aws_iam_access_key.ses.id}'
export GNAR_SES_REGION_NAME='${var.aws_region}'
export GNAR_SES_SECRET_ACCESS_KEY='${aws_iam_access_key.ses.secret}'

export GNAR_SQS_ACCESS_KEY_ID='${aws_iam_access_key.sqs.id}'
export GNAR_SQS_REGION_NAME='${var.aws_region}'
export GNAR_SQS_SECRET_ACCESS_KEY='${aws_iam_access_key.sqs.secret}'

>>>>> End environment variables <<<<<
STRING
}

output "iam_kops_access_key_id" {
  value = "${aws_iam_access_key.kops.id}"
}

output "iam_kops_secret_access_key" {
  value = "${aws_iam_access_key.kops.secret}"
}
