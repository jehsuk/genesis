# https://www.terraform.io/docs/providers/aws/r/ecr_repository.html

resource "aws_ecr_repository" "powder" {
  name = "powder"
}

resource "aws_ecr_repository" "piste" {
  name = "piste"
}

resource "aws_ecr_repository" "off-piste" {
  name = "off-piste"
}

# https://www.terraform.io/docs/providers/aws/r/ecr_lifecycle_policy.html

resource "aws_ecr_lifecycle_policy" "powder" {
  repository = "${aws_ecr_repository.powder.name}"

  policy = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Expire Old",
            "selection": {
                "tagStatus": "any",
                "countType": "imageCountMoreThan",
                "countNumber": 1
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF
}

resource "aws_ecr_lifecycle_policy" "piste" {
  repository = "${aws_ecr_repository.piste.name}"

  policy = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Expire Old",
            "selection": {
                "tagStatus": "any",
                "countType": "imageCountMoreThan",
                "countNumber": 1
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF
}

resource "aws_ecr_lifecycle_policy" "off-piste" {
  repository = "${aws_ecr_repository.off-piste.name}"

  policy = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Expire Old",
            "selection": {
                "tagStatus": "any",
                "countType": "imageCountMoreThan",
                "countNumber": 1
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF
}
